using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization.Metadata;
using System.Text.RegularExpressions;

namespace StudioKit.ErrorHandling.WebApi;

public static class ProblemDetailsUtils
{
	public static string TitleForCode(int? statusCode)
	{
		if (!statusCode.HasValue) return null;
		var httpStatusCode = (HttpStatusCode)statusCode;
		return httpStatusCode switch
		{
			HttpStatusCode.InternalServerError => "Error",
			// get the name of the code and insert spaces before each
			_ => Regex.Replace(httpStatusCode.ToString(), "([a-z])([A-Z])", "$1 $2")
		};
	}

	public static string MessageForCode(int? statusCode)
	{
		if (!statusCode.HasValue) return null;
		var httpStatusCode = (HttpStatusCode)statusCode;
		return httpStatusCode switch
		{
			HttpStatusCode.Unauthorized => "Your request is unauthorized. You must authenticate.",
			HttpStatusCode.Forbidden => "Your request has been denied. You do not have access.",
			HttpStatusCode.NotFound => "The requested content was not found.",
			HttpStatusCode.InternalServerError => "An error has occurred.",
			_ => null
		};
	}

	public static ObjectResult CreateProblemDetailsObjectResult(ProblemDetails problemDetails)
	{
		var objectResult = new ObjectResult(problemDetails)
		{
			StatusCode = problemDetails.Status
		};
		var jsonSerializerOptions = new JsonSerializerOptions
		{
			TypeInfoResolver = new DefaultJsonTypeInfoResolver()
		};
		// force problemDetails to be serialized using System.Text.Json, to use it's built in JsonConverter
		objectResult.Formatters.Add(new SystemTextJsonOutputFormatter(jsonSerializerOptions));
		return objectResult;
	}
}