using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Linq;

namespace StudioKit.ErrorHandling.WebApi;

/// <summary>
/// <para>
/// A custom implementation of <see cref="ProblemDetailsFactory"/> which provides default values for
/// <see cref="ProblemDetails.Title"/> and <see cref="ProblemDetails.Detail"/> and adds legacy error message properties.
/// </para>
/// <para>
/// See <see cref="DefaultProblemDetailsFactory"/>.
/// </para>
/// </summary>
public class CustomProblemDetailsFactory : ProblemDetailsFactory
{
	private const string LegacyCodeKey = "code";
	private const string LegacyMessageKey = "message";

	private readonly ApiBehaviorOptions _options;

	public CustomProblemDetailsFactory(IOptions<ApiBehaviorOptions> options)
	{
		_options = options?.Value ?? throw new ArgumentNullException(nameof(options));
	}

	public override ProblemDetails CreateProblemDetails(
		HttpContext httpContext,
		int? statusCode = null,
		string title = null,
		string type = null,
		string detail = null,
		string instance = null)
	{
		statusCode ??= 500;
		title ??= ProblemDetailsUtils.TitleForCode(statusCode);
		detail ??= ProblemDetailsUtils.MessageForCode(statusCode);

		var problemDetails = new ProblemDetails
		{
			Status = statusCode,
			Type = type,
			Title = title,
			Detail = detail,
			Instance = instance
		};
		// add legacy properties
		problemDetails.Extensions.Add(LegacyCodeKey, statusCode);
		problemDetails.Extensions.Add(LegacyMessageKey, detail);

		ApplyProblemDetailsDefaults(httpContext, problemDetails, statusCode.Value);

		return problemDetails;
	}

	public override ValidationProblemDetails CreateValidationProblemDetails(
		HttpContext httpContext,
		ModelStateDictionary modelStateDictionary,
		int? statusCode = null,
		string title = null,
		string type = null,
		string detail = null,
		string instance = null)
	{
		if (modelStateDictionary == null)
		{
			throw new ArgumentNullException(nameof(modelStateDictionary));
		}

		statusCode ??= 400;
		title ??= ProblemDetailsUtils.TitleForCode(statusCode);
		detail ??= ProblemDetailsUtils.MessageForCode(statusCode);

		var problemDetails = new ValidationProblemDetails(modelStateDictionary)
		{
			Status = statusCode,
			Type = type,
			Detail = detail,
			Instance = instance
		};

		if (title != null)
		{
			// For validation problem details, don't overwrite the default title with null.
			problemDetails.Title = title;
		}

		// add legacy properties
		problemDetails.Extensions.Add(LegacyCodeKey, statusCode);
		problemDetails.Extensions.Add(LegacyMessageKey, string.Join(",\r\n",
			problemDetails.Errors.Select(kvp =>
				$"{kvp.Key}: {string.Join("; ", kvp.Value)}"
			)));

		ApplyProblemDetailsDefaults(httpContext, problemDetails, statusCode.Value);

		return problemDetails;
	}

	private void ApplyProblemDetailsDefaults(HttpContext httpContext, ProblemDetails problemDetails, int statusCode)
	{
		problemDetails.Status ??= statusCode;

		if (_options.ClientErrorMapping.TryGetValue(statusCode, out var clientErrorData))
		{
			problemDetails.Title ??= clientErrorData.Title;
			problemDetails.Type ??= clientErrorData.Link;
		}

		var traceId = Activity.Current?.Id ?? httpContext?.TraceIdentifier;
		if (traceId != null)
		{
			problemDetails.Extensions["traceId"] = traceId;
		}
	}
}