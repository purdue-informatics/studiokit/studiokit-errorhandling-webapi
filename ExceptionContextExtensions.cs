﻿using Microsoft.AspNetCore.Mvc.Filters;
using StudioKit.ErrorHandling.Interfaces;

namespace StudioKit.ErrorHandling.WebApi;

public static class ExceptionContextExtensions
{
	public static IExceptionUser GetExceptionUser(this ExceptionContext exceptionContext)
	{
		return exceptionContext.HttpContext.User.Identity.ToExceptionUser();
	}
}