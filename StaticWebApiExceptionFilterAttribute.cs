﻿using Microsoft.AspNetCore.Mvc.Filters;
using StudioKit.ErrorHandling.Interfaces;
using System;

namespace StudioKit.ErrorHandling.WebApi;

/// <summary>
/// An exception filter to handle all System.Web.Http namespace exceptions (e.g. Web API).
/// Requires the static <see cref="ErrorHandler.Instance"/> to be set in order to work.
/// </summary>
public class StaticWebApiExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilterAttribute
{
	public override void OnException(ExceptionContext exceptionContext)
	{
		base.OnException(exceptionContext);

		var exception = exceptionContext.Exception;
		if (IsExceptionIgnored(exception))
			return;

		ErrorHandler.Instance?.CaptureException(exception, exceptionContext.GetExceptionUser());
	}

	/// <summary>
	/// Override point for defining which exceptions or types of exceptions are ignored.
	/// </summary>
	/// <param name="exception">The exception</param>
	/// <returns></returns>
	public virtual bool IsExceptionIgnored(Exception exception)
	{
		return exception is OperationCanceledException;
	}
}